var nconf = require('nconf');
var fs = require('fs');
var sh = require('execSync');

nconf.argv({
	"username" : {
		describe: 'Username to build under',
		demand: true
	},
});
console.log("Removing all images for user : "+nconf.get('username')+"\n");
var cmd = "docker rmi --force $(docker images --no-trunc -qa "+nconf.get('username')+"/*)";
console.log(cmd);
var code = sh.run(cmd);
console.log("Removal Completed.\n");
