var nconf = require('nconf');
var async = require('async');
var fs = require('fs');
var sh = require('execSync');
var dockerFile,code;

nconf.argv({
	"url" : {
		alias:'location',
		describe: 'URL of Git Repo',
		demand: true
	},
	"username" : {
		describe: 'Username to build under',
		demand: true
	},
	"name" : {
		describe: 'Name of build',
		demand: true
	},
	"port" : {
		describe: 'Service port to expose',
		default:0
	},
	"sshport" : {
		describe: 'Port for SSH Server',
		alias:'ssh',
		default:0
	}
});

function cleanFS(callback) {
	code = sh.run('rm -rf /home/node/converter/worktable');
	code = sh.run('mkdir /home/node/converter/worktable');
	console.log('Filesystem Cleaned.');
	callback(null);
}

function gitFS(callback) {
	console.log('Downloading repo.');
	code = sh.run('git clone '+nconf.get('url')+' /home/node/converter/worktable/temp');
	console.log('Repo downloaded.');
	callback(null);
}

function buildDocker(callback) {
	dockerFile = "FROM docker.io/alpine:latest\nRUN apk add --update nodejs\nCOPY package.json /src/package.json\nRUN cd /src; npm install\nCOPY . /src\n";

	if (nconf.get('sshport') != 0) { //If SSH port specified?
		dockerFile += "RUN apk add dropbear\nRUN rc-update add dropbear\nCOPY dropbear /etc/conf.d/dropbear";
		var dropBear = "DROPBEAR_BANNER='NodeDocker Container'\nDROPBEAR_PORT="+nconf.get('sshport')+"\n";
		fs.writeFileSync("/home/node/converter/worktable/temp/dropbear",dropBear);
		console.log('DropBear write completed.');
	}
	if (nconf.get('port') != 0) {
		dockerFile += "EXPOSE "+nconf.get('port')+" "+nconf.get('sshport')+"\n";
	}

	dockerFile += 'CMD ["node", "/src/index.js"]';
	dockerFile += "\n";
	fs.writeFileSync("/home/node/converter/worktable/temp/Dockerfile",dockerFile);
	console.log('Dockerfile write completed.');
	callback(null);
}

function doBuild(callback) {
	//Now build the image
	console.log('Building image');
	code = sh.run('docker build -t registry.nodedocker:5000/'+nconf.get('username')+'/'+nconf.get('name')+' /home/node/converter/worktable/temp');
	console.log('Build completed.');
	callback(null);
}


//Build

async.series([
	cleanFS,
	gitFS,
	buildDocker,
	doBuild
], function(err) {
	if(err) console.log(err);
	console.log('Script completed.');
});

